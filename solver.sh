#!/bin/bash
# set -x # print commands as they're being executed
set -eu

required="openssl sed awk zip"

for program in $required; do
  which "${program}" >/dev/null || {
    echo "$program is required to run this script. Exiting."
    exit 1
  }
done

if [[ "$#" -ne 3 ]]; then
  echo "Usage: $0 INDEX FIRST_NAME LAST_NAME"
  exit 1
fi

index="$1"
name="$2"
last_name="$3"

# 1.1

echo -n "" > index.txt
echo -n "00" > serial
mkdir -p newcerts

# 1.2

# TODO: fake times

openssl req -x509 -newkey rsa -nodes -days 3652 -subj "/C=PL/ST=Pomorskie/O=PG WETI/CN=${index}_CA" -out CA.cer
mv privkey.pem CA.key

# 1.3

cp /usr/lib/ssl/openssl.cnf .

sed -i 's/^dir\s*=.*/dir = ./' openssl.cnf
sed -i 's/^private_key\s*=.*/private_key = CA.key/' openssl.cnf
sed -i 's/# keyUsage/keyUsage/' openssl.cnf
sed -i 's/cacert.pem/CA.cer/' openssl.cnf

# 2.1

openssl req -new -nodes -subj "/C=PL/ST=Pomorskie/O=PG WETI/CN=${index}_${last_name}_${name}" -keyout klient.key -out klient.req

# 2.2

openssl ca -config openssl.cnf -batch -days 730 -in klient.req -out klient.cer

# 3.3

openssl pkcs12 -export -passout pass:lab142 -inkey klient.key -in klient.cer -certfile CA.cer -out "${index}.p12"

# 4.3

# This makes sure times are being reported with a dot instad of a comma.
export LC_NUMERIC=en_US.UTF-8

# This makes the `time` builtin only report the real time and nothing else.
TIMEFORMAT="%R"

benchmark() {
  local max=0

  eval $1
  for _ in {1..10}; do
    # set +x # necessary if set -x was enabled
    local time_taken=$({ time $1 1>/dev/null 2>/dev/null; } 2>&1)
    # set -x # turns command debugging back on
    if (( $(echo "${time_taken} > ${max}" | bc -l) )); then
      max="${time_taken}"
    fi
  done

  echo "${max}"
}

aes_encrypt_time=$(benchmark "openssl enc -e -aes-256-cbc -nosalt -pass pass:1234 -in files/dane.pdf -out aes.enc")
aes_decrypt_time=$(benchmark "openssl enc -d -aes-256-cbc -nosalt -pass pass:1234 -in aes.enc -out aes.pdf")

des_encrypt_time=$(benchmark "openssl enc -e -des-cbc -nosalt -pass pass:1234 -in files/dane.pdf -out des.enc")
des_decrypt_time=$(benchmark "openssl enc -d -des-cbc -nosalt -pass pass:1234 -in des.enc -out des.pdf")

aes_encrypt_size=$(ls -l aes.enc | awk '{print $5}')
aes_decrypt_size=$(ls -l aes.pdf | awk '{print $5}')
des_encrypt_size=$(ls -l des.enc | awk '{print $5}')
des_decrypt_size=$(ls -l des.pdf | awk '{print $5}')

# 4.2

cat <<EOF > "${index}.txt"
ALGORYTM	Z/O	CZAS		WIELKOSC
DES-CBC		Z	${des_encrypt_time}			${des_encrypt_size}
DES-CBC		O	${des_decrypt_time}			${des_decrypt_size}
AES-256-CBC	Z	${aes_encrypt_time}			${aes_encrypt_size}
AES-256-CBC	O	${aes_decrypt_time}			${aes_decrypt_size}


###############################################################
Wykonać pomiar przy zaszyfrowywaniu i odszyfrowywaniu dla każdego z algorytmów szyfrujących.
Podać:
- AGORYTM - nazwa algorytmu.
- Z/O - informacja czy pomiar dotyczy operacji zaszyfrowywania (Z) czy odszyfrowywania (O).
- CZAS - wynik pomiaru czasu operacji. Jeśli polecenie pomiarowe podaje kilka wartości, wpisać największą.
- WIELKOSC - wielkość pliku wynikowego danej operacji (dokładna, w bajtach).
EOF

# 5.1

openssl smime -sign -inkey klient.key -signer klient.cer -in "${index}.txt" -out smime_detach.signed
openssl smime -sign -nodetach -inkey klient.key -signer klient.cer -in "${index}.txt" -out smime_nodetach.signed

# 5.3

sed -i 's/AGORYTM/ALGORYTM/' smime_detach.signed

# 5.4

if openssl smime -verify -signer klient.cer -CAfile CA.cer -in smime_detach.signed > smime_detach.ver 2>&1; then
  echo "smime_detach.signed verfication was expected to fail but succeeded."
  echo "Exiting."
  exit 1
fi

openssl smime -verify -signer klient.cer -CAfile CA.cer -in smime_nodetach.signed > smime_nodetach.ver 2>&1

# 6.2

archive="${index}_${last_name}_${name}.zip"

# 1
zip -r "${archive}" serial index.txt newcerts/ CA.key CA.cer openssl.cnf

# 2
zip "${archive}" klient.key klient.req klient.cer

# 3
zip "${archive}" "${index}.p12"

# 4
zip "${archive}" "${index}.txt"

# 5
zip "${archive}" smime_detach.signed smime_nodetach.signed smime_detach.ver smime_nodetach.ver

# 6.3

openssl smime -encrypt -binary -aes-256-cbc -in "${archive}" -out "${archive}.enc" files/lab-pki.crt
openssl smime -sign -signer klient.cer -inkey klient.key -in "${archive}.enc" -out "${index}_${last_name}_${name}.enc"

if openssl smime -verify -signer klient.cer -CAfile CA.cer -in "${index}_${last_name}_${name}.enc"; then
  echo "ZIP signed sucessfully."
else
  echo "Couldn't verify signed ZIP file."
  exit 1
fi
